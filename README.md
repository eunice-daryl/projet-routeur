#   JELLYFIN-DE


-------------------------------------------------VM AZURE----------------------------------------------------------

## CONNEXION
```
PS C:\Users\Usager> ssh azureuser@20.123.65.151
Welcome to Ubuntu 20.04.6 LTS (GNU/Linux 5.15.0-1059-azure x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/pro

  System information as of Tue Mar 26 10:57:51 UTC 2024

  System load:  0.0               Processes:             103
  Usage of /:   5.3% of 28.89GB   Users logged in:       0
  Memory usage: 32%               IPv4 address for eth0: 10.0.0.4
  Swap usage:   0%

Expanded Security Maintenance for Applications is not enabled.

0 updates can be applied immediately.

Enable ESM Apps to receive additional future security updates.
See https://ubuntu.com/esm or run: sudo pro status



The programs included with the Ubuntu system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Ubuntu comes with ABSOLUTELY NO WARRANTY, to the extent permitted by
applicable law.

To run a command as administrator (user "root"), use "sudo <command>".
See "man sudo_root" for details.

```

## INSTALLATION
```
azureuser@VM-Azure:~$ sudo apt install curl gnupg
curl is already the newest version (7.68.0-1ubuntu2.21).
gnupg is already the newest version (2.2.19-3ubuntu2.2).
```
- Activation du référentiel Universe sur Ubuntu (et dérivés) uniquement
ruby

```
azureuser@VM-Azure:~$ sudo add-apt-repository universe
'universe' distribution component is already enabled for all sources.
```

Téléchargement et installation de la clé de signature GPG (signée par l'équipe Jellyfin)
```
azureuser@VM-Azure:~$ sudo mkdir -p /etc/apt/keyrings
azureuser@VM-Azure:~$ curl -fsSL https://repo.jellyfin.org/jellyfin_team.gpg.key | sudo gpg --dearmor -o /etc/apt/keyrings/jellyfin.gpg

```
- configuration du référentiel Jellyfin 

```
azureuser@VM1:~$ sudo nano /etc/apt/sources.list.d/jellyfin.sources
ensuite on rajoute export VERSION_OS="$( awk -F'=' '/^ID=/{ print $NF }' /etc/os-release )"
export VERSION_CODENAME="$( awk -F'=' '/^VERSION_CODENAME=/{ print $NF }' /etc/os-release )"
export DPKG_ARCHITECTURE="$( dpkg --print-architecture )"
cat <<EOF | sudo tee /etc/apt/sources.list.d/jellyfin.sources
Types: deb
URIs: https://repo.jellyfin.org/${VERSION_OS}
Suites: ${VERSION_CODENAME}
Components: main
Architectures: ${DPKG_ARCHITECTURE}
Signed-By: /etc/apt/keyrings/jellyfin.gpg
EOF à l'intérieur
```

- Mise a jour de l'APT (Advanced Package Tool)

```
azureuser@VM-Azure:~$ sudo apt update
Get:1 http://azure.archive.ubuntu.com/ubuntu focal InRelease [265 kB]
Get:2 http://azure.archive.ubuntu.com/ubuntu focal-updates InRelease [114 kB]

azureuser@VM1:~$ sudo apt install jellyfin-server jellyfin-web
Reading package lists... Done
Building dependency tree
Reading state information... Done
jellyfin-web is already the newest version (10.8.13-1).
jellyfin-web set to manually installed.

```

```
azureuser@VM-Azure:~$ sudo nano /etc/jellyfin/server.xml
<ServerConfiguration>
    <WebClient>
        <BaseUrl>http://localhost:8096/web</BaseUrl>
        <UpdateChannel>release</UpdateChannel>
        <UpdatePolicy>default</UpdatePolicy>
        <Path>C:\Users\Usager\Downloads\Music-T</Path>
    </WebClient>
</ServerConfiguration>
azureuser@VM-Azure:~$

```
Pour vous assurer que pare-feu autorise le trafic sur le port 8096 et que votre routeur est correctement configuré pour rediriger le trafic du port 8096 vers le serveur Jellyfin:

```
azureuser@VM-Azure:~$ sudo ufw allow 8096/tcp
Rules updated
Rules updated (v6)
azureuser@VM-Azure:~$
/Music/urls.txt

```

```
azureuser@VM-Azure:~$ mkdir Film
azureuser@VM-Azure:~$ ls
Film  Music  install-debuntu.sh  jellyfin  jellyfin-key.asc
azureuser@VM-Azure:~$ sudo systemctl restart jellyfin

```

```

azureuser@VM-Azure:~$ mkdir Clip-Music
azureuser@VM-Azure:~$ sudo systemctl restart jellyfin
azureuser@VM-Azure:~$ sudo service jellyfin start
azureuser@VM-Azure:~$ sudo service jellyfin start
azureuser@VM-Azure:~$ fdisk -l

```

```
azureuser@VM-Azure:~$ sudo fdisk -l
Disk /dev/loop0: 63.93 MiB, 67010560 bytes, 130880 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes


Disk /dev/loop1: 91.85 MiB, 96292864 bytes, 188072 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes


Disk /dev/loop2: 39.1 MiB, 40996864 bytes, 80072 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes


Disk /dev/sda: 30 GiB, 32213303296 bytes, 62916608 sectors
Disk model: Virtual Disk
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 4096 bytes
I/O size (minimum/optimal): 4096 bytes / 4096 bytes
Disklabel type: gpt
Disk identifier: EFFA454D-4DC2-4A20-A4E8-223723CF2943

Device      Start      End  Sectors  Size Type
/dev/sda1  227328 62916574 62689247 29.9G Linux filesystem
/dev/sda14   2048    10239     8192    4M BIOS boot
/dev/sda15  10240   227327   217088  106M EFI System

Partition table entries are not in disk order.


Disk /dev/sdb: 4 GiB, 4294967296 bytes, 8388608 sectors
Disk model: Virtual Disk
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 4096 bytes
I/O size (minimum/optimal): 4096 bytes / 4096 bytes
Disklabel type: dos
Disk identifier: 0x83375fdd

Device     Boot Start     End Sectors Size Id Type
/dev/sdb1        2048 8386559 8384512   4G  7 HPFS/NTFS/exFAT
azureuser@VM-Azure:~$

```

```

azureuser@VM-Azure:~$ sudo blkid
/dev/sda1: LABEL="cloudimg-rootfs" UUID="fb8d1de5-d160-4ed5-97d5-976e1ec09b1f" TYPE="ext4" PARTUUID="b787777b-8807-4aee-8856-0dcf8a637ed2"
/dev/sda15: LABEL_FATBOOT="UEFI" LABEL="UEFI" UUID="7100-7162" TYPE="vfat" PARTUUID="4cd7904e-2463-4522-8d77-a23f6f19edcc"
/dev/sdb1: UUID="44ee9a01-5503-4582-acab-992d516f6332" TYPE="ext4" PARTUUID="83375fdd-01"
/dev/loop0: TYPE="squashfs"
/dev/loop1: TYPE="squashfs"
/dev/loop2: TYPE="squashfs"
/dev/sda14: PARTUUID="20595b0e-62b7-4e72-b0e6-4620eb5d4703"
azureuser@VM-Azure:~$ cd /
azureuser@VM-Azure:/$ ls
bin   etc   lib32   lost+found  music  root  snap  tmp
boot  home  lib64   media       opt    run   srv   usr
dev   lib   libx32  mnt         proc   sbin  sys   var
azureuser@VM-Azure:/$ cd / media
-bash: cd: too many arguments
azureuser@VM-Azure:/$ cd /media
azureuser@VM-Azure:/media$ ls
azureuser@VM-Azure:/media$

```


```
azureuser@VM-Azure:/media$ sudo mkdir SSD
azureuser@VM-Azure:/media$ ls
SSD
azureuser@VM-Azure:/media$ sudo nano /etc/fstab
azureuser@VM-Azure:/media$ sudo nano /etc/fstab



Use "fg" to return to nano.

[1]+  Stopped                 sudo nano /etc/fstab
azureuser@VM-Azure:~$ sudo mkdir -p /media/SDD
azureuser@VM-Azure:~$ ls
Clip-Music  Film  Music  install-debuntu.sh  jellyfin  jellyfin-key.asc
azureuser@VM-Azure:~$ sudo nano /etc/fstab



azureuser@VM-Azure:~$ sudo cat /etc/fstab
# /etc/fstab: static file system information.


# CLOUD_IMG: This file was created/modified by the Cloud Image build process
UUID=fb8d1de5-d160-4ed5-97d5-976e1ec09b1f       /        ext4   defaults,discard        0 1
UUID=7100-7162  /boot/efi       vfat    umask=0077      0 1
/dev/disk/cloud/azure_resource-part1    /mnt    auto    defaults,nofail,x-systemd.requires=cloud-init.service,_netdev,comment=cloudconfig       0       2

/dev/disk/by-uuid/fb8d1de5-d160-4ed5-97d5-976e1ec09b1f /media/SDD       ext4 defaults     0     0
azureuser@VM-Azure:~$ sudo mount -a
azureuser@VM-Azure:~$
azureuser@VM-Azure:~$
```

```
azureuser@VM-Azure:~$ sudo mkdir Shows
azureuser@VM-Azure:~$ sudo systemctl restart jellyfin
azureuser@VM-Azure:~$ sudo nano /etc/systemd/logind.conf
HandleSuspendKey=ignore
HandleLidSwitch=ignore
HandleLidSwitchDocked=ignore
azureuser@VM-Azure:~$ sudo systemctl restart systemd-logind
```


